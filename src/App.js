import React from 'react'
import './App.css'

const App = () => (
  <div className='app'>
    <h1 className='heading'>React Boilerplate</h1>
    <p>With Webpack and Babel</p>
  </div>
)

export default App
