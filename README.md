# React Webpack Boilerplate

A Boilerplate for React Apps, that uses **webpack**([docs](https://webpack.js.org/concepts/)) and **Babel**([docs](https://babeljs.io/docs/en/)) instead of _Create React App_

## Install

`yarn` or `npm install`

## Start

`yarn start` or `npm start`

## Build

`yarn build` or `npm build`
